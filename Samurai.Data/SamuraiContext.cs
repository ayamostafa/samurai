﻿using Microsoft.EntityFrameworkCore;
using Samurai.Domain;

namespace Samurai.Data
{
    public class SamuraiContext : DbContext
    {
        public DbSet<Samurai.Domain.Samurai> samurais { get; set; }
        public DbSet<Quote> quotes { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=SamuraiAppDataFirstLook;");

        }
    }
}
